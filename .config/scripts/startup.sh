#!/bin/bash
# Display Startup

# A script to execute at startup which checks for current network config and
# sets up displays according based on the network location.

logfile="$HOME/.config/hypr/hyprland-display.log"
monitorconfig="$HOME/.config/hypr/hyprland.conf.d/monitors.conf"
paperconfig="$HOME/.config/hypr/hyprpaper.conf"
dockconf=""
dockpaper=""
homeconf=""
homepaper=""
workconf=""
workpaper=""
mobileconf=""
mobilepaper=""

# Get the status of thunderbolt devices $boltstatus
#source getboltstatus.sh

# Remove symlinks from prior boot
rm $monitorconfig
rm $paperconfig
rm $logfile

#if [[ "$boltstatus" == "authorized" ]]; then
	#echo "Thunderbolt dock connected." > $logfile
	#ln -s $HOME/.config/hypr/hyprland.conf.d/hyprland-home.conf $hyprconfig
	#ln -s $HOME/.config/hypr/hyprpaper.conf.d/hyprpaper-home.conf $paperconfig	
#fi

# Check if connected to Ethernet
if ip link show eth0 | grep -q "state UP"; then
	echo "Ethernet connection established." > $logfile
	ln -s $HOME/.config/hypr/hyprland.conf.d/monitors.d/hyprland-home.conf $monitorconfig
	ln -s $HOME/.config/hypr/hyprpaper.conf.d/hyprpaper-home.conf $paperconfig
else

	#Check for wireless_tools
	if ! command -v iwgetid &> /dev/null; then
		echo "Error: wireless_tools not installed" >> $logfile
		echo "'sudo pacman -S wireless_tools' to install." >> $logfile
		exit 1
	fi

	# Get the current SSID
	ssid=$(iwgetid -r)

	# Set display config for home workspace
	if [[ "$ssid" == "KAVANAGH" || "$ssid" == "Pizza Party" ]]; then
		echo "Connected to home network." >> $logfile
		ln -s $HOME/.config/hypr/hyprland.conf.d/monitors.d/hyprland-home.conf $monitorconfig
		ln -s $HOME/.config/hypr/hyprpaper.conf.d/hyprpaper-home.conf $paperconfig

	# Set display config for work network
	elif [[ "$ssid" = "iHeartGuest" ]]; then
		echo "Connected to work network." >> $logfile
		ln -s $HOME/.config/hypr/hyprland.conf.d/monitors.d/hyprland-work.conf $monitorconfig
		ln -s $HOME/.config/hypr/hyprpaper.conf.d/hyprpaper-work.conf $paperconfig

	# Set display config for mobile usage	
	else
		echo "Connected to mobile network." >> $logfile
		ln -s $HOME/.config/hypr/hyprland.conf.d/monitors.d/hyprland-mobile.conf $monitorconfig
		ln -s $HOME/.config/hypr/hyprpaper.conf.d/hyprpaper-mobile.conf $paperconfig
	fi

fi

echo "Displays setup properly" >> $logfile

#sleep 5

hyprpaper

exit 0
