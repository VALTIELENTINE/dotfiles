# Personal .dotfiles

A collection of personal dotfiles and scripts for my Arch install. Please feel free to borrow from it as you see fit.

![Desktop Screenshot](Pictures/Screenshots/desktop_3.png)

All scripts designed for use with T2 Kernel on an Intel MacBook Pro, check before running!

## Dependencies

```
paru -S <Coming Soon!>
```

## Setup
```sh
git init --bare $HOME/.git/dotfiles
echo "alias gitdots='git --git-dir=$HOME/.git/dotfiles --work-tree=$HOME'" >> $HOME/.zshrc
gitdots remote add origin gitlab@gitlab.com:VALTIELENTINE/dotfiles.git
```

## Replication
```sh
git clone --separate-git-dir=$HOME/.git/dotfiles https://gitlab.com/VALTIELENTINE/dotfiles.git $HOME/.dotfiles
rsync --recursive --verbose --exclude '.git' $HOME/.dotfiles $HOME/
rm --recursive $HOME/.dotfiles
```

## Configuration
```sh
gitdots config status.showUntrackedFiles no
gitdots remote set-url origin gitlab@gitlab.com:VALTIELENTINE/dotfiles.git
```

## Usage
```sh
gitdots status
gitdots add <file | -u>
gitdots commit -m 'Commit Message'
gitdots push
```
