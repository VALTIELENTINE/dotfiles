vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autoread = true

-- set spacing and use spaces instead of tabs
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = false
vim.opt.shiftround = true
vim.opt.wrap = false

-- set line numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- set column at 80 chars
vim.opt.colorcolumn = "80,144,182"
