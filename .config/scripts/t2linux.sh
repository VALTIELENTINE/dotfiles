#!/bin/bash
#T2 Linux Config
#This script copies config files essential for t2linux

ans="q"

## T2 Mac? ##
echo "Are you on a T2 mac? [y/n]"
while [[ "$ans" != "y" && "$ans" != "Y" && "$ans" != "n" && "$ans" != "N" ]]; do
	read -r ans
	if [[ "$ans" == "n" || "$ans" == "N" ]]; then
		exit 1
	fi
done

## Check for cloned git repo in $HOME ##
if [[ -d "$HOME/.config/t2linux" ]]; then
	echo "Initializing config for T2 mac"
	echo "Backing up original config files..."

	## Check for existing backups ##
	if [[ -e "/etc/t2fand.conf.bak" ]]; then
		echo "Fan daemon config already backed up."
		echo "Would you like to replace backups with current config? [y/n]"
		ans="q"
		while [[ "$ans" != "y" && "$ans" != "Y" && "$ans" != "n" && "$ans" != "N" ]]; do
			read -r ans
			if [[ "$ans" == "y" || "$ans" == "Y" ]]; then
				echo "Removing existing backups"
				sudo rm /etc/t2fand.conf.bak
				sudo rm /etc/tiny-dfr/config.toml.bak
				echo "Backing up current config"
				sudo mv /etc/t2fand.conf /etc/t2fand.conf.bak 
				sudo mv /etc/tiny-dfr/config.toml /etc/tiny-dfr/config.toml.bak 
			elif [[ "$ans" == "n" || "$ans" == "N" ]]; then
				echo "Your current config will be deleted. Are you sure you want to continue? [y/n]"
				while [[ "$ans" != "y" && "$ans" != "Y" && "$ans" != "n" && "$ans" != "N" ]]; do
					read -r confirm
					if [[ "$confirm" == "n" || "$confirm" == "N" ]]; then
						exit 0
					elif [[ "$confirm" == "y" || "$confirm" == "Y" ]]; then
						echo "Removing current config files."
						sudo rm /etc/t2fand.conf
						sudo rm /etc/tiny-dfr/config.toml
					fi
				done
			fi
		done
	else
		echo "Backing up current config"
		sudo mv /etc/t2fand.conf /etc/t2fand.conf.bak 
		sudo mv /etc/tiny-dfr/config.toml /etc/tiny-dfr/config.toml.bak 
	fi

	## Copy the new config files ##
	echo "Copying custom config files from '$HOME/.config/t2linux'"
		sudo cp $HOME/.config/t2linux/t2fand.conf /etc/t2fand.conf
		sudo cp $HOME/.config/t2linux/tiny-dfr/config.toml /etc/tiny-dfr/config.toml
	echo "Copying complete. Please reboot your system."
	exit 0
else
	echo "Error. No config files found in '$HOME/.config/t2linux'"
	echo "Make sure you have copied all files from the git repository to your home directory and that you are calling the script from the same user account."
	exit 1
fi

exit 0
